#-------------------------------------------------------------------------------
# Name:        module1
# Purpose:
#
# Author:      Piyush
#
# Created:     01-03-2016
# Copyright:   (c) Piyush 2016
# Licence:     MIT License
#-------------------------------------------------------------------------------

def first(word):
    return word[0]

def last(word):
    return word[-1]

def middle(word):
    return word[1:-1]

def is_palindrome(string):
    """Function to check whether a string is palindrome
    """
    l = len(string)
    if l == 0 or l == 1:
        return True
    return first(string) == last(string) and is_palindrome(middle(string))