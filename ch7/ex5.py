#-------------------------------------------------------------------------------
# Name:        module1
# Purpose:
#
# Author:      Piyush
#
# Created:     01-03-2016
# Copyright:   (c) Piyush 2016
# Licence:     MIT License
#-------------------------------------------------------------------------------

def ack(m, n):
    """Ackermann function
    """
    if m < 0:
        print "M cannot be negative"
        return
    elif m == 0:
        return n + 1
    elif n < 0:
        print "N cannot be negative when M is positive"
    elif n == 0:
        return ack(m - 1, 1)
    else:
        return ack(m - 1, ack(m , n - 1))

m = 3
n = 3

print "Ackermann function for", m, ",", n, "is", ack(m, n)
