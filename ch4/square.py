#-------------------------------------------------------------------------------
# Name:        module1
# Purpose:
#
# Author:      Piyush
#
# Created:     17-02-2016
# Copyright:   (c) Piyush 2016
# Licence:     MIT License
#-------------------------------------------------------------------------------

def main():
    pass

if __name__ == '__main__':
    main()

from swampy.TurtleWorld import *

turtle = TurtleWorld()
bob = Turtle()

fd(bob, 100)
lt(bob)
fd(bob, 100)
lt(bob)
fd(bob, 100)
lt(bob)
fd(bob, 100)


wait_for_user()
