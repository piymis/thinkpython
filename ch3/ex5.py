def do_twice(f):
	f()
	f()

def do_four(f):
	do_twice(f)
	do_twice(f)	

def print_beam():
	print "+ - - - - ",

def print_beams():
	do_twice(print_beam)
	print "+"

def print_vertical():
	print "/         ",

def print_verticals():
	do_twice(print_vertical)
	print"/"	

def print_row():
	print_beams()
	do_four(print_verticals)
def print_rows():
	do_twice(print_row)

def print_grid():
	print_rows()
	print_beams()

def print_row4():
	do_four(print_beam)
	print "+"
	do_four(print_verticals)
def big_grid():
	do_four(print_row4)

#print_grid()
big_grid()	